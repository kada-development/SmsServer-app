package se.kada.smsserver;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.telephony.SmsManager;

import java.util.UUID;

import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpResponse;
import com.koushikdutta.async.http.Multimap;
import com.koushikdutta.async.http.callback.HttpConnectCallback;
import com.koushikdutta.async.http.server.AsyncHttpServer;
import com.koushikdutta.async.http.server.AsyncHttpServerRequest;
import com.koushikdutta.async.http.server.AsyncHttpServerResponse;
import com.koushikdutta.async.http.server.HttpServerRequestCallback;


public class SmsServerService extends Service {
    int mId;
    NotificationManager mNotificationManager;
    AsyncHttpServer server;
    PowerManager.WakeLock wakeLock;

    LogWriter logger;

    /**
     * Interface for service events
     */
    private SmsServerListener listener;

    public interface SmsServerListener {
        void onSmsServerLog(String msg);
        //void onSmsReceived();
        void onSmsServerStart();
        void onSmsServerStop();
        void onSmsServerError(String error); // log, inform activity, create notification, attempt restart, sms alert?
    }

    /**
     * Binder for client to communicate with the sms service
     */
    private final IBinder mBinder = new SmsBinder();

    public class SmsBinder extends Binder {
        int getPort() {
            return Config.port;
        }
        void stop() { server.stop(); }
        void restart() {
            stop();
            start();
        }
        void start() { server.listen(Config.port); }

        void registerSmsListener(SmsServerListener listener) {
            SmsServerService.this.listener = listener;
        }
        //void unregisterSmsListener() {}
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return true;
    }


    @Override
    public void onCreate() {
        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SmsServerWakelock");
        logger = new LogWriter();
        logger.add("Starting SmsServerService");
        startServer();

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle extra = intent.getExtras();
                //TODO log better
                if (extra.get("message") != null) {
                    String message = extra.get("message").toString();
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            addLog(String.format("Message sent:\n%s", message));
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            addLog(String.format("Message failed:\n%s", message));
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            addLog(String.format("Message failed:\n%s", message));
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            addLog(String.format("Message failed:\n%s", message));
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            addLog(String.format("Message failed:\n%s", message));
                            break;
                    }
                }
                if (extra.get("uuid") != null) {
                    String uuid = extra.get("uuid").toString();
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            sendResponse(uuid, "sent");
                            addLog(String.format("SMS with uuid %s was sent ", uuid));
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            sendResponse(uuid, "generic_failure");
                            addError("Generic failure");
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            sendResponse(uuid, "no_service");
                            addError("No service");
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            sendResponse(uuid, "null_pdu");
                            addError("Null PDU");
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            sendResponse(uuid, "radio_off");
                            addError("Radio off");
                            break;
                    }
                }
            }
        }, new IntentFilter(SmsSender.SENT));

        //---when the SMS has been delivered---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                //TODO log better
                Bundle extra = intent.getExtras();
                if (extra.get("uuid") != null) {
                    String uuid = extra.get("uuid").toString();
                    switch (getResultCode())
                    {
                        case Activity.RESULT_OK:
                            addLog(String.format("SMS with uuid %s delivered", uuid));
                            sendResponse(uuid, "delivered");
                            break;
                        case Activity.RESULT_CANCELED:
                            addError(String.format("SMS with uuid %s was canceled", uuid));
                            sendResponse(uuid, "canceled");
                            break;
                    }
                }
                if (extra.get("message") != null) {
                    String message = extra.get("message").toString();
                    switch (getResultCode())
                    {
                        case Activity.RESULT_OK:
                            addLog(String.format("Message delivered:\n%s", message));
                            break;
                        case Activity.RESULT_CANCELED:
                            addLog(String.format("Message canceled:\n%s", message));
                            break;
                    }
                }

            }
        }, new IntentFilter(SmsSender.DELIVERED));

        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        getWakelock();
        showNotification();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        logger.add("Destroying server");
        System.out.println("Destroying server!");
        hideNotification();
        server.stop();
        if (wakeLock.isHeld())
            wakeLock.release();
        super.onDestroy();
    }


    private void sendResponse(String uuid, String message) {
        AsyncHttpClient client = AsyncHttpClient.getDefaultInstance();
        String httpUrl = String.format("%s?uuid=%s&message=%s", Config.URL, uuid, message);
        client.execute(httpUrl, new HttpConnectCallback() {
            public void onConnectCompleted(Exception e, AsyncHttpResponse resp) {
                if (e != null) {
                    e.printStackTrace();
                    return;
                }
            }
        });
    }

    private void startServer() {
        final Context context = this;
        server = new AsyncHttpServer();

        server.get("/", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                response.send("Hello World!");
                addLog("Received a request on '/', responded with Hello World");
            }
        });

        server.get("/messages", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {

                addLog("Received a GET request on '/messages',sending SMS");
                Multimap query = request.getQuery();
                String phoneNo = query.getString("to");
                String message = query.getString("message");

                if (phoneNo != null && message != null) {
                    String uuid = SmsSender.getDefault().send(context, phoneNo, message);
                    addLog(String.format("Sending sms with uuid: %s to %s with message:\n%s", uuid, phoneNo, message));
                    response.send(uuid);
                } else {
                    addError("Missing parameters, cannot send SMS");
                    response.send("Missing parameters");
                }
            }
        });

        server.setErrorCallback(new CompletedCallback() {
            @Override
            public void onCompleted(Exception ex) {
                addError(ex.getMessage());
            }
        });

        // listen on port
        server.listen(Config.port);
        addLog("Listening on port " + Config.port);
    }

    private void addLog(String log) {
        logger.add(log);
        if (listener != null) {
            listener.onSmsServerLog(log);
        }
    }
    private void addError(String error) {
        logger.add("ERROR: " + error);
        if (listener != null) {
            listener.onSmsServerError(error);
        }
    }
    private void getWakelock() {
        if (!wakeLock.isHeld())
            wakeLock.acquire();
    }

    private void showNotification() {

        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_action_settings_cell)
                        .setContentTitle("SMS Server")
                        .setContentText("Currently running")
                        .setOngoing(true);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, LogActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(LogActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        // mId allows you to update the notification later on.
        Notification notification = mBuilder.build();
        mNotificationManager.notify(mId, notification);
        startForeground(mId, notification);
    }

    private void hideNotification() {
        mNotificationManager.cancel(mId);
    }

}

