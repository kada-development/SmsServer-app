package se.kada.smsserver;

/**
 * Created by maistho on 2016-09-16.
 */
public class Config {
    public final static String[] phoneNumbers = {"+46768630333"};
    public final static String URL = "http://10.37.0.13:8080";
    public final static int port = 5000;
    public final static String logLocation = "SmsServer/sms.log";
    public final static int startLogLines = 10;
    public final static int maxLogLines = 20;
    public final static int SMSDelay = 2500;
}
