package se.kada.smsserver;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.telephony.SmsManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by maistho on 2016-09-18.
 */
public class SmsQueue extends Thread {
    private Handler handler;

    public final static String SENT = "se.kada.smsserver.SMS_SENT";
    public final static String DELIVERED = "se.kada.smsserver.SMS_DELIVERED";

    @Override
    public void run() {
        try {
            Looper.prepare();

            handler = new Handler();

            Looper.loop();
        } catch (Throwable t) {
            Log.e("halted due to an error", t.getMessage());
        }
    }

    private AtomicInteger counter = new AtomicInteger(0);
    private long lastRun = 0;

    public synchronized ArrayList<String> enqueueSms(final Context context, final String to, final String message) {
        final SmsManager sms = SmsManager.getDefault();
        System.out.println(SmsManager.MMS_CONFIG_MESSAGE_TEXT_MAX_SIZE);
        final ArrayList<String> messages = sms.divideMessage(message);
        final ArrayList<String> uuids = new ArrayList<>(messages.size());
        final ArrayList<PendingIntent> sentPI = new ArrayList<>(messages.size());
        final ArrayList<PendingIntent> deliveredPI = new ArrayList<>(messages.size());

        // Create sent/delivery intents and UUIDs
        for (int i = 0; i < messages.size(); ++i) {
            String uuid = UUID.randomUUID().toString();
            uuids.add(uuid);

            Intent sentIntent = new Intent(SENT);
            sentIntent.putExtra("uuid", uuid);
            sentIntent.putExtra("message", message);
            sentPI.add(PendingIntent.getBroadcast(
                    context,
                    counter.incrementAndGet(),
                    sentIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT));

            Intent deliveryIntent = new Intent(DELIVERED);
            deliveryIntent.putExtra("uuid", uuid);
            deliveryIntent.putExtra("message", message);
            deliveredPI.add(PendingIntent.getBroadcast(
                    context,
                    counter.incrementAndGet(),
                    deliveryIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT));
        }

        handler.post(new Runnable() {
            @Override
            public void run() {
                long current = System.currentTimeMillis();
                long diff = current - lastRun;
                if (diff < Config.SMSDelay) {
                    try {
                        currentThread().sleep(Config.SMSDelay - diff);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                lastRun = System.currentTimeMillis();


                //sms.sendTextMessage(to, null, message, sentPI, deliveredPI);
                sms.sendMultipartTextMessage(to, null, messages, sentPI, deliveredPI);
            }
        });

        return uuids;
    }
}
