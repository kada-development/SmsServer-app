package se.kada.smsserver;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;

public class LogReader {

    private boolean debug = true;

    private int runEveryNSeconds = 1000;
    private long lastKnownPosition = 0;
    private boolean shouldIRun = true;
    private File mFile = null;
    private int counter = 0;

    public LogReader(File logFile) {

        mFile = logFile;
    }

    public ArrayList<String> tail(File file, int lines) {
        // base of parsing from http://stackoverflow.com/a/7322581
            java.io.RandomAccessFile fileHandler = null;
            try {
                fileHandler =
                        new java.io.RandomAccessFile( file, "r" );
                long fileLength = fileHandler.length() - 1;
                StringBuilder sb = new StringBuilder();
                int line = 0;

                for(long filePointer = fileLength; filePointer != -1; filePointer--){
                    fileHandler.seek( filePointer );
                    int readByte = fileHandler.readByte();

                    if( readByte == 0xA ) {
                        if (filePointer < fileLength) {
                            line = line + 1;
                        }
                    } else if( readByte == 0xD ) {
                        if (filePointer < fileLength-1) {
                            line = line + 1;
                        }
                    }
                    if (line >= lines) {
                        break;
                    }
                    sb.append( ( char ) readByte );
                }

                String lastLine = sb.reverse().toString();

                return new ArrayList<>(Arrays.asList(lastLine.split("\n|\r")));
            } catch( java.io.FileNotFoundException e ) {
                e.printStackTrace();
                return null;
            } catch( java.io.IOException e ) {
                e.printStackTrace();
                return null;
            }
            finally {
                if (fileHandler != null)
                    try {
                        fileHandler.close();
                    } catch (IOException e) {
                    }
            }
    }
}
