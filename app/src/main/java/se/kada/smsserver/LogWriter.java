package se.kada.smsserver;

import android.os.Environment;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by maistho on 2016-08-13.
 */
public class LogWriter {
    File mFile;
    FileWriter mWriter;
    public LogWriter(File file) {
        mFile = file;
        try {
            mWriter = new FileWriter(mFile, true);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("Could not open file for writing.");
        }
    }

    public LogWriter(String filename) {
        this(new File(filename));
    }
    public LogWriter() {
        this(new File(Environment.getExternalStorageDirectory(), Config.logLocation));

    }


    public void add(String message) {
        System.out.println("Logger: " + message);
        try {
            mWriter.append(message + "\n");
            mWriter.flush();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("Could not write to file...");
        }
    }
}
