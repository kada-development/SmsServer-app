package se.kada.smsserver;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by maistho on 2016-09-16.
 */
public class SmsSender extends Thread {
    public final static String SENT = "se.kada.smsserver.SMS_SENT";
    public final static String DELIVERED = "se.kada.smsserver.SMS_DELIVERED";

    private static SmsSender singleton;

    private SmsQueue queue;
    public SmsSender() {
        queue = new SmsQueue();
        queue.start();
    }


    public static SmsSender getDefault() {
        if (singleton == null)
            singleton = new SmsSender();
        return singleton;
    }

    public String send(Context context, final String phoneNumber, final String message)
    {
        return "[\"" + TextUtils.join("\", \"", queue.enqueueSms(context, phoneNumber, message)) + "\"]";
    }

}
