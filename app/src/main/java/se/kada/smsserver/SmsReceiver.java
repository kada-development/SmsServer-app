package se.kada.smsserver;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

import java.util.UUID;

public class SmsReceiver extends BroadcastReceiver {

    public SmsReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                String from = smsMessage.getOriginatingAddress();
                String messageBody = smsMessage.getMessageBody();
                String message = String.format("Message from: %s\n%s", from, messageBody);

                for (int i = 0; i < Config.phoneNumbers.length; ++i) {
                    SmsSender.getDefault().send(context, Config.phoneNumbers[i], message);
                }
            }
        }
    }
}
