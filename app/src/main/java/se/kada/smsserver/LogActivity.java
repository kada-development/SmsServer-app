package se.kada.smsserver;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import se.kada.smsserver.SmsServerService.SmsServerListener;
import se.kada.smsserver.SmsServerService.SmsBinder;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class LogActivity extends AppCompatActivity implements SmsServerListener {

    private SmsBinder smsBinder;
    ArrayAdapter<String> adapter;
    ArrayList<String> logTail;

    private ServiceConnection mSmsConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            smsBinder = (SmsBinder)iBinder;
            smsBinder.registerSmsListener(LogActivity.this);
            System.out.println("Connected to SmsService");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            System.out.println("Disconnected from SmsService");
        }
    };

    ListView mLogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        mLogView = (ListView)findViewById(R.id.logView);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    private static final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.SEND_SMS
    };
    private static final int PERMISSION_ALL = 1;

    private boolean retryPermissions = true;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length == 0) {
                    // TODO: show alert for error
                    System.out.println("No length");

                    ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
                    return;
                } else {
                    for (int i = 0; i < grantResults.length; ++i) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            // TODO: show alert for error

                            System.out.println("No permissions");
                            if (retryPermissions) {
                                retryPermissions = false;
                                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
                            } else {
                                Context context = getApplicationContext();
                                CharSequence text = "No permissions granted. You need to grant SmsServer all the permissions it asks for in order for it to work!";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();

                                this.finishAffinity();
                            }
                            return;
                        }
                    }
                    initialize();
                }
            }
        }
    }


    @Override
    protected void onStart() {
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            initialize();
        }

        super.onStart();
    }

    private void initialize() {
        Intent intent = new Intent(this, SmsServerService.class);
        startService(intent);
        bindService(intent, mSmsConnection, Context.BIND_ABOVE_CLIENT);

        TextView connectionInfo = (TextView)findViewById(R.id.connectionInfo);

        String ipAddress = NetworkUtils.getIPAddress(true);
        connectionInfo.setText(String.format("Connect to http://%s:%s", ipAddress, Config.port));

        File sdcard = Environment.getExternalStorageDirectory();
        File logFile = new File(sdcard, Config.logLocation);
        try {
            logFile.getParentFile().mkdirs();
            logFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        LogReader reader = new LogReader(logFile);
        logTail = reader.tail(logFile, Config.startLogLines);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, logTail);
        mLogView.setAdapter(adapter);


        // Go to bottom of log
        mLogView.setSelection(adapter.getCount()-1);
    }

    @Override
    protected void onStop() {
        try {
            unbindService(mSmsConnection);
        } catch(IllegalArgumentException e) {
            System.out.println("Could not stop service because it was not bound");
        }
        super.onStop();
    }

    private void addToLog(String log) {
        logTail.add(log);
        if (logTail.size() > Config.maxLogLines)
            logTail.remove(0);

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                mLogView.setSelection(adapter.getCount()-1);
            }
        });
    }

    /**
     * SmsServerListener
     */
    @Override
    public void onSmsServerLog(String msg) {
        addToLog(msg);
    }

    @Override
    public void onSmsServerStart() { }

    @Override
    public void onSmsServerStop() { }

    @Override
    public void onSmsServerError(String error) {
        addToLog("ERROR: " + error);
    }
}
