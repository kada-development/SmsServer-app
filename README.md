# SmsServer app

This application can act as a gateway-server for sending and receiving Sms 
messages. It will run on Android 4.4+ and is built using Android Studio.

## Usage
All user-configurable settings are found in `Config.java`.

Change these to your and build the app using Android Studio. At first launch you
will need to grant permissions to the app. If you do not grant permissions the
app will not work.

Connect to the app on the IP adress of your phone. We recommend setting a static
IP adress for the phone. The IP adress and port is shown in the app for convenience.

## API

### Sending a message

`/messages?to=<number>&message=<message>`

where `<number>` is the number receiving the message and `<message>` is a string
containing your message. Messages should be URL-encoded to avoid issues with
reserved characters, and should be sent using UTF-8. Emojis and other
[Unicode-nonsense](https://eeemo.net/) is also fine.

The app will respond with an array of UUIDs, one for each message. There 
can be multiple messages to allow for multipart sms, where each message is
split around 160 characters.

### Callbacks

The app will send a callback with the following formatting to the URL configured in `Config.java`.

`<Config.URL>?uuid=<uuid>&message=<status>`

Where `<uuid>` is an UUID previously sent as a response by the app, and
`<status>` is a status message.

#### Status messages
Successful status messages:
- `sent`: Indicates that the message was sent from the phone.
- `delivered`: Indicates that the message was delivered to the recipient.

Failed status messages:
- `generic_failure`: Indicates a failure with sending the message. Please file an issue if you encounter this error.
- `no_service`: Failed because the service is currently unavailable.
- `null_pdu`: Failed because no PDU was found. Make sure that you can send messages manually using the same phone.
- `radio_off`: Failed because radio was turned off. You are probably in Airplane mode.
- `canceled`: Failed to deliver because the message was canceled.

